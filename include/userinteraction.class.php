<?
class userIteraction extends _db{
	//@integer, Счетчик
	private $i;
	
	//@integer, Счетчик
	private $j;
	
	//@array, Массив для перебора результатов текущего топа
	private $resultTop;
	
	//@array, Массив для перебора 100 юзеров  текущего топа
	private $resultUsers;
	
	//@array, вспомогательный массив для записи значений для каждой итерации
	private $arr;

	//@array, Массив для записи нового топа перед отправкой его в БД
	private $arrResult;
	
	// @string, Переменная для хранения запроса к БД
	private $sql;
	
	// @string, Переменная для хранения результата обработки запроса к БД
	private $result;
	
	//Добавить какое-то количество пользователей с рандомным количеством очков в таблицу users
	public function addUsers($userCount){
		for ($this->i = 1; $this->i <= $userCount; $this->i++) {
			$this->arrRand[] = rand(1,1000);
		}
		foreach ($this->arrRand as $v){
			$this->query("INSERT INTO users (score) VALUES ($v)");
		}
	}
	
	//Добавление очков пользователю
	public function scoreAdd($id,$addValue){
		$this->sql = "UPDATE users SET score=score+$addValue WHERE id = $id";
		$this->result = $this->query($this->sql);
		if ($this->result == 0){
			$this->sql = "INSERT INTO users (id, score) VALUES ($id, $addValue);";
			$this->query($this->sql);
		}
	}
	
	//Пересчет топа из таблицы users, сохранение старого топа в таблицу old_top
	public function calculate(){
		$this->sql ="SELECT * FROM users ORDER BY score DESC limit 100";
		$this->resultUsers = $this->query($this->sql);
		$this->sql ="SELECT * FROM top";
		$this->resultTop = $this->query($this->sql);
		$this->i=0;
		if (empty ($this->resultTop)){
			foreach($this->resultUsers as $user){
				$this->i++;
				$this->query("INSERT INTO top (id,place,score,diff) VALUES ({$user['id']},{$this->i},{$user['score']},0);");
			}
		}
		else{
			$this->i=0;
			foreach ($this->resultUsers as $user){
				$this->i++;
				$this->j=0;
				foreach ($this->resultTop as $top){
					$this->j++;
					if ($user['id'] == $top['id']){
						$this->arr['id']=$user['id'];
						$this->arr['place']=$this->i;
						$this->arr['score']=$user['score'];
						$this->arr['diff']=$top['place']-$this->i;
						$this->arrResult[] = $this->arr;
						break;
					}
					if ($this->j==100){
						$this->arr['id']=$user['id'];
						$this->arr['place']=$this->i;
						$this->arr['score']=$user['score'];
						$this->arr['diff']=1000;
						$this->arrResult[] = $this->arr;
						break;
					}
				}
			}
			$this->sql="TRUNCATE TABLE old_top;";
			$this->query($this->sql);
			$this->sql="INSERT INTO old_top SELECT * FROM top";
			$this->query($this->sql);
			$this->sql="TRUNCATE TABLE top;";
			$this->query($this->sql);
			foreach($this->arrResult as $v){
				$this->query("INSERT INTO top (id,place,score,diff) VALUES ({$v['id']},{$v['place']},{$v['score']},{$v['diff']});");
			}
		}
	}
	
	//Вывод топа
	public function getTop(){
		$this->sql = "SELECT * FROM top ORDER BY place";
		$this->result = $this->query($this->sql);
		echo "Текущий топ";
		echo "<table border='1px'>";
			echo "<tr>";
				echo "<th>";
					echo "User";
				echo "</th>";
				echo "<th>";
					echo "place";
				echo "</th>";
				echo "<th>";
					echo "score";
				echo "</th>";
				echo "<th>";
					echo "diff";
				echo "</th>";
			echo "</tr>";
			foreach($this->result as $v){
				echo "<tr>";
					echo "<td>";
						echo $v['id'];
					echo "</td>";
					echo "<td>";
						echo $v['place'];
					echo "</td>";
					echo "<td>";
						echo $v['score'];
					echo "</td>";	
					echo "<td>";
						echo $v['diff'];
					echo "</td>";	
				echo "</tr>";
					}
		echo "</table>";
	}
	
	//Вывод прошлого топа
	public function getOldTop(){
		$this->sql = "SELECT * FROM old_top ORDER BY place";
		$this->result = $this->query($this->sql);
		echo "Текущий топ";
		echo "<table border='1px'>";
			echo "<tr>";
				echo "<th>";
					echo "User";
				echo "</th>";
				echo "<th>";
					echo "place";
				echo "</th>";
				echo "<th>";
					echo "score";
				echo "</th>";
				echo "<th>";
					echo "diff";
				echo "</th>";
			echo "</tr>";
			foreach($this->result as $v){
				echo "<tr>";
					echo "<td>";
						echo $v['id'];
					echo "</td>";
					echo "<td>";
						echo $v['place'];
					echo "</td>";
					echo "<td>";
						echo $v['score'];
					echo "</td>";	
					echo "<td>";
						echo $v['diff'];
					echo "</td>";	
				echo "</tr>";
					}
		echo "</table>";
	}
}
?>