<?php
error_reporting(E_ALL);
require("include/dbconn.class.php");
require("include/userinteraction.class.php");
	
/*Добавляем userCount пользователей с рандомными значениями очков в таблицу users*/

if ($_GET['cmd'] == 'addscore'){
	require('addusers.php');
}
if ($_GET['cmd'] == 'addUsers'){
	$users = new userIteraction();
	$users->addUsers($_GET['cnt']);
}

/*Начисление очков*/

if ($_GET['cmd'] == 'score'){
	$score = new userIteraction();
	$score->scoreAdd($_GET['user_id'],$_GET['value']);
	unset($score);
}

/*Пересчет топа*/

if ($_GET['cmd'] == 'calc'){
	$calc = new userIteraction();
	$calc->calculate();
	unset($calc);
}

/*Вывод текущего топа*/
	
if ($_GET['cmd'] == 'top'){
	$top = new userIteraction();
	$top->getTop();
}

/*Вывод прошлого топа*/

if ($_GET['cmd'] == 'oldTop'){
	$top = new userIteraction();
	$top->getOldTop();
}

?>